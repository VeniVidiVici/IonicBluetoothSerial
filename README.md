基于`ionic`的蓝牙通讯框架
=====
- 目前只在Android平台上可用
- `ionic` : <http://ionicframework.com/>
- `bluetoothserial`插件 : <https://github.com/don/BluetoothSerial>
- `ngCordova` : <http://ngcordova.com/>

Service `bluetoothHelper`
-----
封装`bluetoothSerial`的`enable`、`list`等细节，便于其他模块使用

### `init`
- type : `function`
- parameters
	1. `deviceName` : `string` | 要连接的蓝牙名称
- return : `promise` | 连接成功时resolve，失败时reject
- 此函数先开启蓝牙，然后在已配对设备中寻找`deviceName`，最后进行连接。未开启蓝牙、未配对或者连接失败都将导致reject

### `subscribe`
- type : `function`
- parameters
	1. `fn` : `function` | 当蓝牙收到数据时要调用的函数
	2. `length` : `number` | 缓冲的字节长度。在缓冲区长度达到`length`时才调用`fn`
- 注册一个回调函数用于监听蓝牙数据

### `write`
- type : `function`
- parameters
	1. `data` : `ArrayBuffer` or `Uint8Array` or `int[]` or `string` | 要向蓝牙发送的数据
- return : `promise` | 发送成功时resolve，失败时reject

### `disconnect`
- type : `function`
- parameters : none
- return : `promise` | 断开成功时resolve，失败时reject
