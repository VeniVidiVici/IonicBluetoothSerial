angular.module('App', ['ionic', 'ngCordova'])

.run(function ($rootScope, $ionicPlatform) {
    $ionicPlatform.ready(function () {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
        $rootScope.$broadcast('ready', null);
    });
})
;